from django.urls import path
from . import views

urlpatterns = [
    path('', views.HomePageView.as_view(), name='home'),
    path('account/', views.AccountView.as_view(), name='dashboard'),
    path('account/super/', views.SuperView.as_view(), name='super'),
    path('account/register/', views.NewUserView.as_view()),
    path('account/activate/', views.AccountActiveView.as_view()),
    path('account/settings/', views.AccountSettingView.as_view()),
    path('account/notifications/', views.AccountNotificationView.as_view()),
    path('notifications/action/', views.NotificationActionView.as_view()),
    path('notifications/edit/', views.NotificationEditView.as_view()),
    path('notifications/view/', views.NotificationView.as_view()),
    path('user/info/', views.UserInfoView.as_view()),
    path('user/account/edit/', views.AccountEdit.as_view()),
    path('users/query/', views.UsersQ.as_view()),
]
