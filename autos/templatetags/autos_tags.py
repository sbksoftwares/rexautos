from django import template
from django.contrib.auth.models  import User
from autos.models import Notification
import json

register = template.Library()

@register.simple_tag
def all_members():
    return User.objects.exclude(is_superuser=1)

@register.simple_tag(takes_context=True)
def recent_notes(context):
    user = context['request'].user
    notes =  Notification.objects.filter(receiver=user)[:3]
    for note in notes:
        content = json.loads(note.note)
        setattr(note, 'head', content['head'])
        setattr(note, 'body', content['body'][:31])
        setattr(note, 'code', content['code'])
    return notes

