# Generated by Django 2.1.1 on 2018-10-08 05:50

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('autos', '0007_auto_20181007_1956'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='added_by',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='added_by', to=settings.AUTH_USER_MODEL),
        ),
    ]
