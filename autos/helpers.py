import requests
import json

def send_email(msg={}):
    return requests.post("https://api.elasticemail.com/v2/email/send",
                data={"apikey": "3a6ca53e-2902-409c-92b0-00e5b9e37dbc",
                   "from":"sbksoftwares@gmail.com",
                   "to": msg['to'],
                   "subject": msg['subject'],
                   "text": msg['text']
                   })

def activation_note(user, admin, medium, mid):
    return {
        'head':'{} is requesting for activation of account'.format(user),
        'body':'hello {} please activate my account as i"ve made all the necessary commitments, i paid through {} and this is my transaction id {}'.format(admin, medium, mid),
        'code':'activation',
    }

def activation_info_note(user, admin):
    return {
        'head':'{} has activated your account'.format(admin),
        'body':'hello {} your account has now been activated, you can continue buiding your network by adding more people'.format(user),
        'code':'activation',
    }

def notes(note='info', params={}):
    if note == 'activation':
        return activation_note(**params)
    elif note == 'ainfo':
        return activation_info_note(**params)