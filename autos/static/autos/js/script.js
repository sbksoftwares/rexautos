// Globals

var rexautos = {
    navigation: {
        prev: "",
        now: "dashboard"
    },
    navLinks: {
        prev: "",
        now: "dashboard"
    },
    urls: {
        'noted': {
            'url': '/notifications/view/',
            'params': ['noteid']
        },
        'notel': {
            'url': '/account/notifications/',
            'params': ['start', 'stop']
        },
        'viewUser': {
            'url': '/user/info/',
            'params': ['userid']
        },
        'newUser': {
            'url': '/account/register/'
        },
        'editUser': {
            'url': '/user/account/edit/'
        }
    }
};


function executeCallback(func, args) {
    if (typeof func === "function") {
        func(args);
        return true;
    } else {
        throw new TypeError(func + " is not a function");
    }
}

function forbidden(string) {
    const items = string.split('|');
    var f_object = {};

    for (const item of items) {
        var keyval = item.split(':');
        f_object[keyval[0]] = keyval[1].split('~');
    }

    return f_object;

}

function ClickBait(traps) {
    this.traps = traps;

    this.trap = function () {
        $(document).on('click', '.bait', (event) => {
            event.preventDefault();
            event.stopPropagation();
            console.log(event);
            var data = event.currentTarget.dataset;
            console.log(data);
            const traps = forbidden(data.traps);
            const bait = forbidden(data.bait);
            console.log(traps);
            console.log(bait);

            if ('main' in traps) {
                var func, args;
                if ('pre' in traps) {
                    func = this.traps[traps.pre];
                    args = bait.pre;
                    executeCallback(func, args);
                }

                func = this.traps[traps.main];
                args = bait.main;
                executeCallback(func, args);

                if ('post' in traps) {
                    func = this.traps[traps.post];
                    args = bait.post;
                    executeCallback(func, args);
                }

            } else {
                throw new Error("method main not found");
            }

        });
    };
}

function RenderTemplates(renderers) {
    this.renderers = renderers;

    this.render = function (renderer, data) {
        executeCallback(this.renderers[renderer], data);
    };
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function messenger(msg) {
    $('#exampleModal div.modal-body').html(msg);
    $('#exampleModal').modal('show');
}

function userInfo(params) {
    var html = '<div class="card h-50 w-75 px-5 my-5 mx-auto bg-light mx-atuo">\
                    <div class="card-body info">';
    html += '<h4 class="card-title mb-5" style="text-transform:capitalize">'+ params['user name'] +'</h4>';
    for (const param in params) {
        html += '<br><div class="d-flex justify-content-between align-items-center w-100">\
                    <strong class="text-gray-dark" style="text-transform:capitalize">'+ param +'</strong>\
                    <span>'+ params[param] +'</span>\
                </div>';
    }
    html += '<br><br><button type="button" class="btn btn-secondary bait" data-bait="main:" data-traps="main:goBack">Back</button></div></div>';
    return html;
    
}

function status(status){
    var active = '<div class="d-flex align-items-center p-3 my-3 text-white-50 bg-success rounded shadow-sm">\
                    <div class="lh-100">\
                        <h5 class="mb-0 text-white">Active</h5>\
                        <small class="text-monospace">Your account is active, continue to grow.</small>\
                    </div>\
                </div>';
    var inactive = '<div class="d-flex align-items-center p-3 my-3 text-white-50 bg-danger rounded shadow-sm">\
                       <div class="lh-100">\
                            <h5 class="mb-0 text-white">Inactive</h5>\
                            <small class="text-monospace">This user\'s account is inactive, remind them to activate.</small>\
                        </div>\
                    </div>';
    var unattatched = '<div class="d-flex align-items-center p-3 my-3 text-white-50 bg-primary rounded shadow-sm">\
                        <div class="lh-100">\
                            <h5 class="mb-0 text-white">Unattatched</h5>\
                            <small class="text-monospace">You are currently not attatched to any superior.</small>\
                        </div>\
                    </div>';
    var waiting = '<div class="d-flex align-items-center p-3 my-3 text-white-50 bg-success rounded shadow-sm">\
                       <div class="lh-100">\
                            <h5 class="mb-0 text-white">Waiting</h5>\
                            <small class="text-monospace">You account is awaiting activation from admin.</small>\
                        </div>\
                    </div>';
    
    var html = {'active':active,'inactive':inactive,'unattatched':unattatched,'waiting':waiting};
     return html[status];
}

function table(data){
    var html = '<h2>Members</h2>\
                <div class="table-responsive">\
                    <table class="table table-striped table-hover users">\
                        <caption>List some summary data about the user\'s table</caption>\
                        <thead>\
                            <tr>\
                                <th>#</th>\
                                <th>Username</th>\
                                <th>Email Address</th>\
                                <th>Phone Number</th>\
                                <th>Status</th>\
                            </tr>\
                        </thead>\
                        <tbody>';
    
    for (const dat of data) {
        html += '<tr class="bait" data-bait="main:user-view|pre:|post:viewUser~'+dat.id+'~renderUserView" data-traps="main:showPage|pre:deLink|post:renderPage">\
        <td>'+dat.id+'</td>\
        <td>'+dat.username+'</td>\
        <td>'+dat.email+'</td>\
        <td>'+dat.phone+'</td>\
        <td>'+dat.status+'</td>\
    </tr>';
    }
                            
                           
    html += '</tbody></table></div>';
    return html;
}

$(document).ready(function () {
    $('.page.' + rexautos.navigation.now).fadeIn();

    feather.replace();

    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    $('#js-data-user-ajax').select2({
        width: '100%',
        minimumInputLength: 3,
        multiple: true,
        placeholder: 'To',
        ajax: {
            url: '/users/query/',
            dataType: 'json',
        }
    });

    $('#js-data-add-ajax').select2({
        width: '100%',
        minimumInputLength: 3,
        placeholder: 'To',
        ajax: {
            url: '/users/query/',
            dataType: 'json',
        }
    });

    var templates = new RenderTemplates({
        'renderNotel': function (data) {
            $('.notes>.notes-more').remove();

            var html = '';
            var color = '';
            for (const note of data.notes) {
                color = note.status == 'unread' ? 'e83e8c' : '007bff';
                html += '<div class="media text-muted pt-3">\
                            <img data-src="holder.js/32x32?theme=thumb&amp;bg=' + color + '&amp;fg=' + color + '&amp;size=1" alt="32x32" class="mr-2 rounded"\
                                style="width: 32px; height: 32px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2232%22%20height%3D%2232%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2032%2032%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_166328f5a67%20text%20%7B%20fill%3A%23007bff%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A2pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_166328f5a67%22%3E%3Crect%20width%3D%2232%22%20height%3D%2232%22%20fill%3D%22%23007bff%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2211.046875%22%20y%3D%2217.2%22%3E32x32%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"\
                                data-holder-rendered="true">\
                            <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray bait" data-bait="main:noted|pre:notifications|post:noted~' + note.pk + '~renderNoted"\
                                data-traps="main:showPage|pre:setActive|post:renderPage">\
                                <strong class="d-block text-gray-dark">' + note.head + '</strong>' + note.body + ' ...\
                            </p>\
                        </div>';
            }
            $('.page.notel>.notes').append(html);
            html = '<small class="d-block text-right mt-3 notes-more">\
                        <a href="#" class="bait" data-bait="main:notel|pre:notifications|post:notel~' + data.start + '&' + data.stop + '~renderNotel"\
                            data-traps="main:showPage|pre:setActive|post:renderPage">\
                            Load More ...\
                        </a>\
                    </small>';

            $('.notes').append(html);
        },
        'renderNoted': function (data) {
            var html = '<h2 class="card-title mb-5">' + data.head + '</h2>\
            <blockquote class="blockquote">' + data.body + '</blockquote>\
            <div class="mt-5 pt-5">\
                <button type="button" class="btn btn-secondary mr-3 bait" data-bait="main:" data-traps="main:goBack">Close <span data-feather="x"></span></button>\
                <button type="button" class="btn btn-primary bait" data-bait="main:" data-traps="main:goBack">Action <span data-feather="arrow-right"></span></button>\
            </div>';

            $('.page.noted div.card-body').html(html);
        },
        'renderUserView': function(data){
            console.log(data);
            $('.page.user-view').empty();
            $('.page.user-view').append(status(data.user.status));
            $('.page.user-view').append(userInfo(data.user));
            if (data.complexity === 'complex') {
                $('.page.user-view').append(table(data.members));
            }
        },
        'renderModal': function (data) {
            messenger(data);
        }
    });

    function get_data(url, params, func) {
        var request = $.post(url, params);

        request.done(function (data) {
            if (data.status) {
                return templates.render(func, data.data);
            } else {
                messenger(data.msg);
                return false;
            }
        });

        request.fail(function () {
            messenger('Failed Request, possibly due to poor network.');
            return false;
        });
    }

    var clickBait = new ClickBait({
        'showPage': function (args) {
            var page = args[0];

            var current_page = rexautos.navigation.now;
            if (page !== current_page) {
                console.log(page);
                $('.page.' + current_page).hide();
                $('.page.' + page).fadeIn();
                rexautos.navigation.prev = current_page;
                rexautos.navigation.now = page;
            }
        },
        'setActive': function (args) {
            var link = args[0];

            var current_link = rexautos.navLinks.now;
            if (link !== current_link && Boolean(link)) {
                console.log(link);
                $("#topnav .nav-link.bait").filter("." + link).addClass("disabled");
                $("#topnav .nav-link.bait").filter("." + current_link).removeClass("disabled");
                $("#sidenav .nav-link.bait").filter("." + link).addClass("active");
                $("#sidenav .nav-link.bait").filter("." + current_link).removeClass("active");
                rexautos.navLinks.prev = current_link;
                rexautos.navLinks.now = link;
            }
        },
        'goBack': function (args) {
            var link = [rexautos.navLinks.prev];
            var page = [rexautos.navigation.prev];
            clickBait.traps.showPage(page);
            clickBait.traps.setActive(link);
        },
        'renderPage': function (args) {
            console.log(args);
            var url = rexautos.urls[args[0]].url;
            var keys = rexautos.urls[args[0]].params;
            var values = args[1].split('&');
            if (keys.length != values.length) {
                throw new Error(keys + ' mismatch with ' + values + ' cant tell which is which');
            }
            params = {};
            keys.forEach((key, i) => params[key] = values[i]);
            var func = args[2];
            return get_data(url, params, func);
        },
        'submitForm': function (args) {
            console.log(args);
            var form = args[0];
            var url = rexautos.urls[args[1]].url;
            params = $('#' + form).serialize();
            var func = args[2];
            if ($('#' + form).valid()) {
                return get_data(url, params, func);
            } else {
                console.log('Invalid form');
            }
        },
        'deLink': function(){
            rexautos.navLinks.prev = rexautos.navLinks.now;
        }
    });

    clickBait.trap();
    $('#new-user-form').validate({
        submitHandler: function (form) {
            console.log(form);
        },
        rules: {
            username: {
                required: true,
                minlength: 4,
                maxlength: 256,
            },
            email: {
                required: true,
                email: true,
                maxlength: 256,
            },
            firstname: {
                required: true,
                maxlength: 256,
                minlength: 3,
            },
            lastname: {
                required: true,
                maxlength: 256,
                minlength: 3,
            },
            phone: {
                required: true,
                digits: true,
                maxlength: 256,
                minlength: 9
            },
            addedto: {
                required: true,
                maxlength: 256,
            },
        }
    });

    $('#new-user-form').validate({
        submitHandler: function (form) {
            console.log(form);
        },
        rules: {
            email: {
                required: true,
                email: true,
                maxlength: 256,
            },
            firstname: {
                required: true,
                maxlength: 256,
                minlength: 3,
            },
            lastname: {
                required: true,
                maxlength: 256,
                minlength: 3,
            },
            phone: {
                required: true,
                digits: true,
                maxlength: 256,
                minlength: 9
            }
        }
    });

});