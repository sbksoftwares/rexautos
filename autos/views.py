from django.shortcuts import render
from django.http import JsonResponse
from django.views.generic import TemplateView,View
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Setting,Account,Notification
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
import json
from .helpers import send_email,notes

# Create your Views here.

class HomePageView(TemplateView):
    template_name = 'autos/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["settings"] = Setting.objects.all()
        return context

class AccountView(LoginRequiredMixin, TemplateView):
    template_name = 'autos/account.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["settings"] = Setting.objects.all()
        return context

class NewUserView(View):

    def post(self, request, *args, **kwargs):
        if request.POST:
            # we are creating a new user
            addedto = request.POST.get('addedto')
            if addedto:
                user_added_to = User.objects.get(pk=int(addedto))
                if user_added_to.added_to.count() >= 20:
                    return JsonResponse({'status':False, 'msg':'User Full'})
                # can pair two users
                # allowed to make user staff
            else:
                return JsonResponse({'status': False, 'msg': 'addedto field required'})
                # new user is automatically paired to

            if 'username' in request.POST and 'email' in request.POST and 'firstname' in request.POST and 'lastname' in request.POST and 'phone' in request.POST:
                username = request.POST.get('username')
                email = request.POST.get('email')
                first_name = request.POST.get('firstname')
                last_name = request.POST.get('lastname')
                phone = request.POST.get('phone')
                if User.objects.filter(email=email).count() > 0:
                    return JsonResponse({
                        'status':False,
                        'msg':'This email is already in use by another account'
                    })
                if Account.objects.filter(phone=phone).count() > 0:
                    return JsonResponse({
                        'status':False,
                        'msg':'This phone number is already in use by another account'
                    })
                if username and email and first_name and last_name and phone:
                    try:
                        user = User.objects.create_user(username=username,email=email)
                        Account.objects.create(user=user,added_to=user_added_to,added_by=request.user,phone=phone)
                    except Exception:
                        return JsonResponse({'status':False,'msg':'could not create new user, username is already in use'})
                    
                else:
                    return JsonResponse({'status':False,'msg':'Required fields can"t be empty are Required'})
            else:
                return JsonResponse({'status':False,'msg':'All fields are Required'})


            # generate and send set password email request
            msg = {
                'to':user.email,
                'subject':'Account Activation',
                'text':'Click link to activate'
            }
            try:
                send_email(msg)
            except Exception:
                pass
            
            return JsonResponse({'status':True, 'data':'User Account Successfully Created'})
        else:
            return JsonResponse({'status':False,'msg':'Empty POST Request'})


class AccountActiveView(View):

    def post(self, request, *args, **kwargs):
        if request.POST:
            if 'medium' not in request.POST or 'mid' not in request.POST:
                return JsonResponse({'status':False,'msg':'Proof not submited'})
            else:
                medium = request.POST.get('medium')
                mid = request.POST.get('mid')
                if not mid or not medium:
                    return JsonResponse({'status':False,'msg':'Empty proof'})
            # send activation notification to admin
            admins = User.objects.filter(is_superuser=True)
            params = {
                'user':request.user.get_full_name,
                'medium':medium,
                'mid':mid,
            }
            for admin in admins:
                params['admin'] = admin.get_full_name()
                Notification.objects.create(
                    sender=request.user,
                    receiver=admin,
                    mode='action',
                    note=notes('activation',params),
                )
            # send message to user email/phone
            send_email()
            # send message to admin email/phone
            send_email()

            # set user status to waiting
            account = request.user.account
            account.status = 'waiting'
            account.save()
        else:
            return JsonResponse({'status':False,'msg':'Empty POST Request'})


class AccountSettingView(View):

    def post(self, request, *args, **kwargs):
        if request.POST:
            # change settings value as user defined
            # only admin can change settings
            settings = Setting.objects.all()
            for setting in settings:
                if setting.name in request.POST:
                    setting.value = request.POST.get(setting.name)
                    setting.save()
            return JsonResponse({'status':True,'data':''})
        else:
            return JsonResponse({'status':False,'msg':'Empty POST Request'})

class AccountNotificationView(View):

    def post(self, request, *args, **kwargs):
        if request.POST:
            # fetch Notification
            start = int(request.POST.get('start',0))
            stop = int(request.POST.get('stop',4))
            notes = Notification.objects.filter(receiver=request.user)[start:stop]
            if notes:
                notel = []
                for note in notes:
                    content = json.loads(note.note)
                    notel.append({
                        'sender': note.sender.get_full_name(),
                        'status': note.status,
                        'date_sent': note.date_created,
                        'head': content['head'],
                        'body': content['body'][:30],
                        'code': content['code'],
                        'pk': note.pk,
                    })
                data = {
                    'notes':notel,
                    'start':stop,
                    'stop':stop+3,
                }
                return JsonResponse({'status':True,'msg':'Hey, take a look','data':data})
            else:
                return JsonResponse({'status':False,'msg':'No notes for you'})
        else:
            return JsonResponse({'status':False,'msg':'Empty POST Request'})

class NotificationActionView(View):

    def post(self, request, *args, **kwargs):
        if request.POST:
            # execute action for notification in case of action notification
            if not request.POST.get('noteid'):
                return JsonResponse({'status':False,'msg':'urrm, which not are we talking about here'})
            else:
                noteid = request.POST.get('noteid')
                note = Notification.objects.get(pk=int(noteid))

            if not request.POST.get('action_code'):
                return JsonResponse({'status':False,'msg':'action not verified'})
            else:
                action_code = request.POST.get('action_code')

            if action_code == 'activation':
                # activate
                note.receiver.account.status = 'active'
                note.receiver.account.save()
                # set notification status to read
                note.status = 'action'
                # send notification to user
                params = {
                    'user':note.receiver.get_full_name(),
                    'admin':note.sender.get_full_name(),
                }
                Notification.objects.create(
                    sender=note.reciever,
                    receiver=note.sender,
                    mode='info',
                    note=notes('ainfo',params),
                )
                # send msg to user
                send_email()
                return JsonResponse({'status':True,'msg':'User sucessfully activated'})
            else:
                return JsonResponse({'status':False,'msg':'User activation failed'})

        else:
            return JsonResponse({'status':False,'msg':'Empty POST Request'})

class NotificationEditView(View):

    def post(self, request, *args, **kwargs):
        if request.POST:
            # edit notification
            if not request.POST.get('noteid'):
                return JsonResponse({'status':False,'msg':'urrm, which not are we talking about here'})
            else:
                noteid = request.POST.get('noteid')
                note = Notification.objects.get(pk=int(noteid))

            statuses = ('read', 'unread', 'action')
            status = request.POST.get('status')
            if status in statuses:
                note.status = status
                note.save()
                return JsonResponse({'status':True,'msg':'status changed to ...'})
            else:
                return JsonResponse({'status':False,'msg':'invalid status'})

        else:
            return JsonResponse({'status':False,'msg':'Empty POST Request'})

class UserInfoView(View):

    def post(self, request, *args, **kwargs):
        if request.POST:
            userid = int(request.POST.get('userid'))
            try:
                user = User.objects.get(pk=userid)
                complexity = 'complex' if (request.user.is_superuser or request.user.is_staff) else 'simple'
            except ObjectDoesNotExist:
                return JsonResponse({'status':False,'msg':'User does not exist'})
            
            userdat = {
                'user name':user.username,
                'first name':user.first_name,
                'last name':user.last_name,
                'email':user.email,
                'phone':user.account.phone if user.account.phone else None,
                'added to':user.account.added_to.username if user.account.added_to else None,
                'added by':user.account.added_by.username if user.account.added_by else None,
                'joined on':user.date_joined.strftime('%c'),
                'members registered':user.added_to.count(),
                'status':user.account.status,
            }

            registered = []

            if complexity == 'complex':
                members = user.added_to.all()
                for member in members:
                    registered.append({
                        'username':member.user.username,
                        'id':member.user.id,
                        'email':member.user.email,
                        'phone':member.phone,
                        'status':member.status
                    })

            data = {
                'user':userdat,
                'complexity':complexity,
                'members':registered
            }
            # return brief user info
            return JsonResponse({'status':True,'data':data})
        else:
            return JsonResponse({'status':False,'msg':'Empty POST Request'})


class AccountEdit(View):

    def post(self, request, *args, **kwargs):
        if request.POST:
            # edit user account
            user = request.user
            account = request.user.account
            acc_changed = False
            if request.POST.get('status') and request.user.is_superuser:
                account.status = request.POST.get('status')
                acc_changed = True
            
            if request.POST.get('phone'):
                
                if request.POST.get('phone') != account.phone:
                    if not request.POST.get('phone'):
                        return JsonResponse({
                            'status':False,
                            'msg':'Phone number is required'
                        })
                    if Account.objects.filter(phone=request.POST.get('phone')).count() == 0:
                        account.phone = request.POST.get('phone')
                        acc_changed = True
                    else:
                        return JsonResponse({
                            'status':False,
                            'msg':'This phone number is already in use by another account'
                            })

            usr_changed = False
            if user.email != request.POST.get('email'):
                if not request.POST.get('email'):
                    return JsonResponse({
                        'status':False,
                        'msg':'email is required'
                    })
                if User.objects.filter(email=request.POST.get('email')).count() == 0:
                    user.email = request.POST.get('email')
                    usr_changed = True
                else:
                    return JsonResponse({
                        'status':False,
                        'msg':'This email is already in use by another account'
                    })
            if user.first_name != request.POST.get('firstname'):
                user.first_name = request.POST.get('firstname')
                usr_changed = True

            if user.last_name != request.POST.get('lastname'):
                user.last_name = request.POST.get('lastname')
                usr_changed = True
                        
                    

            if usr_changed:
                user.save()
            
            if acc_changed:
                account.save()
            
            return JsonResponse({
                'status':True,
                'data':'Changes made, if you changed you email, please check your email account for the verification of your email.'
                })
        else:
            return JsonResponse({'status':False,'msg':'Empty POST Request'})


class NotificationView(View):

    def post(self, request, *args, **kwargs):
        if request.POST:
            # view notification
            if not request.POST.get('noteid'):
                return JsonResponse({'status':False,'msg':'urrm, which note are we talking about here'})

            try:
                note = Notification.objects.get(pk=int(request.POST.get('noteid')))
                note.status = 'read'
                note.save()
            except ObjectDoesNotExist:
                return JsonResponse({'status':False,'msg':'this notification does not exist'})
            
            content = json.loads(note.note)
            data = {
                'sender': note.sender.get_full_name(),
                'status': note.status,
                'date_sent': note.date_created,
                'head': content['head'],
                'body': content['body'],
                'code': content['code'],
            }

            return JsonResponse({'status':True,'data':data})

        else:
            return JsonResponse({'status':False,'msg':'Empty POST Request'})


class UsersQ(View):

    def get(self, request, *args, **kwargs):
        term = request.GET.get('term')
        users = User.objects.filter(username__icontains=term)
        results = []
        for user in users:
            results.append({
                'id':user.pk,
                'text':user.username
            })
        
        data = {
            'results':results,
            'pagination':{
                'more':False
            }
        }

        return JsonResponse(data)

class SuperView(View):

    temp_name = 'registration/one_time_auth.html'
    redir_temp_name = 'registration/one_time_success.html'

    def get(self, request, *args, **kwargs):
        # check limit, check pass
        return render(request, template_name=self.temp_name)

    def post(self, request, *args, **kwargs):
        # check limit, check pass
        if request.POST:
            if 'username' in request.POST and 'email' in request.POST and 'firstname' in request.POST and 'lastname' in request.POST and 'phone' in request.POST:
                username = request.POST.get('username')
                email = request.POST.get('email')
                first_name = request.POST.get('firstname')
                last_name = request.POST.get('lastname')
                phone = request.POST.get('phone')
                password = request.POST.get('password').strip()
                confirm = request.POST.get('confirm').strip()
                if password != confirm:
                    form = {'error':['passwords must match']}
                    return render(request, template_name=self.temp_name,context=form)
                if User.objects.filter(email=email).count() > 0:
                    form = {'error':['This email is already in use by another account']}
                    return render(request, template_name=self.temp_name,context=form)
                if Account.objects.filter(phone=phone).count() > 0:
                    form = {'error':['This phone is already in use by another account']}
                    return render(request, template_name=self.temp_name,context=form)
                if username and email and first_name and last_name and phone:
                    try:
                        user = User.objects.create_user(username=username,email=email,first_name=first_name,last_name=last_name,password=password,is_superuser=True,is_staff=False)
                        Account.objects.create(user=user,phone=phone,status='active')
                    except Exception:
                        form = {'error':['something went wrong']}
                        return render(request, template_name=self.temp_name,context=form)
                else:
                    form = {'error':['Required fields can"t be empty are Required']}
                    return render(request, template_name=self.temp_name,context=form)

                return render(request, template_name=self.redir_temp_name)
            else:
                form = {'error':['All fields are Required']}
                return render(request, template_name=self.temp_name,context=form)


            # # generate and send set password email request
            # msg = {
            #     'to':user.email,
            #     'subject':'Account Activation',
            #     'text':'Click link to activate'
            # }
            # try:
            #     send_email(msg)
            # except Exception:
            #     pass            
        else:
            form = {'error':['All fields are Required']}
            return render(request, template_name=self.temp_name,context=form)