from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Account(models.Model):
    ACCOUNT_STATUS = (
        ('inactive','Account not active'),
        ('waiting','Account has initiated activate request'),
        ('unattatched','Account not currently under anyone'),
        ('active','Account is active'),
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    status = models.CharField(max_length=64, choices=ACCOUNT_STATUS, default='inactive')
    phone = models.CharField(max_length=64,default='0244 444 444')
    added_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='added_by')
    added_to = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='added_to')

    def members(self):
        members = map(lambda member: member.user, self.user.added_to.all())
        return list(members)


class Notification(models.Model):
    NOTIFICATION_MODE = (
        ('info','To Inform'),
        ('action','Requiring Action'),
    )

    NOTIFICATION_STATUS = (
        ('unread','User has not seen notification'),
        ('read','User has seen notification'),
        ('action','User has taken action'),
    )

    sender = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True,null=True, related_name='notes_sent')
    receiver = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='notes_recvd')
    status = models.CharField(max_length=64, choices=NOTIFICATION_STATUS, default='unread')
    mode = models.CharField(max_length=64, choices=NOTIFICATION_MODE)
    note = models.TextField()
    date_created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-date_created']


class History(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL,null=True)
    ref = models.CharField(max_length=64)
    note = models.TextField()
    date_created = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)


class Setting(models.Model):
    user_max_attatchment = models.PositiveIntegerField(default=20)
    super_limit = models.PositiveIntegerField(default=3)
    super_pass = models.CharField(max_length=64,default='Super')
    last_modified = models.DateTimeField(auto_now=True)